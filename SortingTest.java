package sortingtest;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Тестовое задание для собеседование - реализация алгоритма сортировки
 *
 * @author kynew
 */
public class SortingTest {

    /**
     * Запуск примера сортировки
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        // перенаправляем поток системы - ввод
        Scanner input = new Scanner(System.in);

        // заполняем массив для сортировки
        ArrayList<Integer> toSort = new ArrayList<Integer>();
        int size = 10;
        try {
            System.out.println("Введите элементы массива: ");
            for (int i = 0; i < size; i++) {
                toSort.add(i, input.nextInt());
            }
        } catch (Exception ex) {
            System.out.println("Нужно вводить целые числа!");
            return;
        }

        // сортируем массив
        ArrayList sorted = sort(toSort);

        // печатаем отсортированный
        for (int i = 0; i < size; i++) {
            System.out.print(sorted.get(i) + " ");
        }

        // освобождаем ресурсы
        input.close();
    }

    /**
     * Алгоритм сортировки вставками
     *
     * @param toSort - неотсортированный массив
     * @return - отсортированный массив
     */
    private static ArrayList sort(ArrayList<Integer> toSort) {
        int size = toSort.size(); // размер массива
        int temp; // хранилище для переставляемого значения
        for (int i = 0; i < size; i++) {
            temp = toSort.get(i);
            int j = i - 1;
            while (j >= 0 && toSort.get(j) > temp) {
                toSort.set(j + 1, toSort.get(j));
                j--;
            }
            toSort.set(j + 1, temp);
        }
        return toSort;
    }

}
